
# Evaluación SCM

Este un sitio web para mostrar el poder del SCM aplicado al sitio web ficticio de una empresa de desarrollo de software.

El repositorio se puede ver desde cero, pero es preferible verlo con las intrucciones de abajo.

## Desarrollo

### Requisitos

Antes de correr el programa, se debe instalar un par de cosas: `node` y `yarn`.

### Instalación

Despues de tener instalados los requisitos debemos correr el comando `yarn` en la terminal.

### Desarrollo

 Despues para levantar el sitio debemos correr el comando `yarn start` en la terminal.